package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.FinanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FinanceRepository extends JpaRepository<FinanceEntity, String> {
    List<FinanceEntity> findAllByStudentId(String studentId);
}
