package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.SchoolEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SchoolRepository extends JpaRepository <SchoolEntity, String> {
    SchoolEntity findFirstByClassId(String classId);
}

