package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.AnswerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface AnswerRepository extends JpaRepository <AnswerEntity, Long> {
    AnswerEntity findFirstByQuestionId(long questionId);
    List<AnswerEntity> findAllByQuestionId(long questionId);
    AnswerEntity deleteByQuestionId(long questionId);
    void deleteAllByQuestionId(long questionId);


}

