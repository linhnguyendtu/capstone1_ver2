package com.example.capdemo1.repository;

import com.example.capdemo1.model.entity.QuestionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuestionRepository extends JpaRepository <QuestionEntity, Long> {
    List<QuestionEntity> findAllByParentId(String parentId);
    QuestionEntity findFirstByQuestionId(long questionId);
    void deleteAllByQuestionId(long questionId);
    List<QuestionEntity> findAllByClassId(String classId);

}

