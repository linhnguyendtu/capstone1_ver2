package com.example.capdemo1.controller;


import com.example.capdemo1.model.reponse.FinanceResponse;
import com.example.capdemo1.service.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/study")
@CrossOrigin("http://localhost:4200")
public class FinanceController {

    @Autowired
    public FinanceService financeService;

    @GetMapping("/hoc-phi/{parentId}")
    public List<FinanceResponse> getFinanceByParentId (@PathVariable(value = "parentId") String parentId){
        return financeService.getFinanceByParentId(parentId);

    }

}
