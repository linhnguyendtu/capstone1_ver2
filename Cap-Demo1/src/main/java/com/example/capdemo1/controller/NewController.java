package com.example.capdemo1.controller;

import com.example.capdemo1.model.entity.NewEntity;
import com.example.capdemo1.model.reponse.NewResponse;
import com.example.capdemo1.model.request.NewRequest;
import com.example.capdemo1.repository.NewRepository;
import com.example.capdemo1.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/news")
@CrossOrigin("http://localhost:4200")
public class NewController {
    @Autowired
    public NewService newService;

    @Autowired
    private NewRepository newRepository;

    @PostMapping("/add-new")
    public ResponseEntity<String> addNew(@RequestBody NewRequest newRequest) {
        newService.saveNew(newRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping("/all-new")
    public List<NewResponse> getAllNew() {
        return newService.getAllNew();
    }

    @GetMapping("/category-id/{id}")
    public List<NewResponse> getNewByCategory(@PathVariable(value = "id") Long id){
        return newService.getNewByCategory(id);
    }
    @GetMapping("/{id}")
    public NewResponse getNewDetail(@PathVariable(value = "id") Long id){
        return newService.getNewDetail(id);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteNew(@PathVariable(value = "id") Long id){
        newService.deleteNew(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<NewResponse> updateNew(@PathVariable(value="id") Long id, @RequestBody NewRequest newRequest){
        newService.updateNew(id, newRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
