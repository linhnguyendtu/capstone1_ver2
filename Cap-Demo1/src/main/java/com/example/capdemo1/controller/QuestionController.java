package com.example.capdemo1.controller;


import com.example.capdemo1.common.Enums;
import com.example.capdemo1.model.reponse.QuestionResponse;
import com.example.capdemo1.model.reponse.ResponseModel;
import com.example.capdemo1.model.request.AnswerRequest;
import com.example.capdemo1.model.request.QuestionRequest;
import com.example.capdemo1.service.QuestionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/question")
@CrossOrigin("http://localhost:4200")
public class QuestionController {

    @Autowired
    public QuestionService questionService;

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionController.class);

    @PostMapping("/create-question")
    public ResponseEntity<String> postQuestion(@RequestBody QuestionRequest questionRequest){
        questionService.postQuestion(questionRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/answer-question")
    public ResponseModel<?> answerQuestion(@RequestBody QuestionRequest questionRequest){
        LOGGER.info("START answerQuestion - QuestionController");
        ResponseModel<?> responseModel = new ResponseModel<>();
        try {
            responseModel =  questionService.answerQuestion(questionRequest);
        } catch (Exception exception) {
            responseModel.setStatus(Enums.Status.ERROR.getValue());
            responseModel.setMessage(exception.getMessage());
        }
        LOGGER.info("START answerQuestion - QuestionController");
        return responseModel;
    }

    @PostMapping("/answer-question")
    public ResponseModel<?> postAnswerQuestion(@RequestBody AnswerRequest answerRequest){
        LOGGER.info("START postAnswerQuestion - QuestionController");
        ResponseModel<?> responseModel = new ResponseModel<>();
        try{
            responseModel = questionService.postAnswerQuestion(answerRequest);
        }
        catch (Exception exception){
            responseModel.setStatus(Enums.Status.ERROR.getValue());
            responseModel.setMessage(exception.getMessage());
        }
        LOGGER.info("END answerQuestion - QuestionController");
        return responseModel;
    }

    @GetMapping("/my-question/{parentId}")
    public List<QuestionResponse> getQuestionByParentId(@PathVariable(value = "parentId") String parentId){
        return questionService.getQuestionByParentId(parentId);
    }
    @PutMapping("/my-question/{parentId}/{questionId}")
    public ResponseModel<?> putAnswerQuestion(@RequestBody QuestionRequest questionRequest, @PathVariable(value = "questionId") String questionId){
        LOGGER.info("START putAnswerQuestion - QuestionController");
        ResponseModel<?> responseModel = new ResponseModel<>();
        try {
            responseModel =  questionService.putAnswerQuestion(questionRequest,questionId);
        } catch (Exception exception) {
            responseModel.setStatus(Enums.Status.ERROR.getValue());
            responseModel.setMessage(exception.getMessage());
        }
        LOGGER.info("START putAnswerQuestion - QuestionController");
        return responseModel;
    }

    @DeleteMapping("/my-question/{parentId}/{questionId}")
    public ResponseModel<?> deleteQuestion(@PathVariable(value = "questionId") long questionId){
        LOGGER.info("START deleteQuestion - QuestionController");
        ResponseModel<?> responseModel = new ResponseModel<>();
        try {
            responseModel =  questionService.deleteQuestion(questionId);
        } catch (Exception exception) {
            responseModel.setStatus(Enums.Status.ERROR.getValue());
            responseModel.setMessage(exception.getMessage());
        }
        LOGGER.info("START deleteQuestion - QuestionController");
        return responseModel;
    }



}
