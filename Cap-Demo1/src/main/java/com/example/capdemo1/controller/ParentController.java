package com.example.capdemo1.controller;


import com.example.capdemo1.model.reponse.ParentResponse;
import com.example.capdemo1.model.reponse.StudentForParentResponse;
import com.example.capdemo1.model.request.ParentRequest;
import com.example.capdemo1.service.ParentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;


@RestController
@RequestMapping("/api/study")
@CrossOrigin("http://localhost:4200")
public class ParentController {

    @Autowired
    public ParentService parentService;

    @GetMapping("/thong-tin-phu-huynh/{parentId}")
    public StudentForParentResponse getParentInformation(@PathVariable(value = "parentId") String parentId){
        return parentService.getParentInformation(parentId) ;
    }

    @PutMapping("/thong-tin-phu-huynh/{parentId}")
    public ResponseEntity<ParentResponse> updateParentInformation(@PathVariable(value = "parentId") String parentId, @RequestBody ParentRequest parentRequest) throws ParseException {
        parentService.updateParentInformation(parentId,parentRequest);
        return new ResponseEntity<>(HttpStatus.OK) ;
    }


}
