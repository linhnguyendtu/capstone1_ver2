package com.example.capdemo1.controller;


import com.example.capdemo1.model.reponse.ClassResponse;
import com.example.capdemo1.model.reponse.ClassStudentResponse;
import com.example.capdemo1.model.reponse.PhoneResponse;
import com.example.capdemo1.model.reponse.QuestionResponse;
import com.example.capdemo1.model.request.SMSRequest;
import com.example.capdemo1.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/teacher")
@CrossOrigin("http://localhost:4200")
public class TeacherController {

    @Autowired
    public TeacherService teacherService;

    @GetMapping("/{teacherId}")
    public List<ClassResponse> getClassByTeacherId(@PathVariable(value = "teacherId") String teacherId) {
        return teacherService.getClassByTeacherId(teacherId);
    }

    @GetMapping("/class/{classId}")
    public List<ClassStudentResponse> getStudentByClassId(@PathVariable(value = "classId") String classId) {
        return teacherService.getStudentByClassId(classId);
    }

    @PostMapping("/send-sms/{classId}")
    public void sendSMS(@PathVariable(value = "classId") String classId, @RequestBody SMSRequest smsRequest) {
        teacherService.sendSMS(classId, smsRequest);
    }

    @GetMapping("/test/{classId}")
    public List<PhoneResponse> getPhone(@PathVariable(value = "classId") String classId) {
        return teacherService.getPhone(classId);
    }

    @GetMapping("/question/{classId}")
    public List<QuestionResponse> getQuestionByClassId(@PathVariable(value = "classId") String classId) {
        return teacherService.getQuestionByClassId(classId);
    }
}
