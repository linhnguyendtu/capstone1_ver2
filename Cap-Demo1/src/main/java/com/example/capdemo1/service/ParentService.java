package com.example.capdemo1.service;

import com.example.capdemo1.model.entity.ParentEntity;
import com.example.capdemo1.model.entity.StudentEntity;
import com.example.capdemo1.model.reponse.StudentForParentResponse;
import com.example.capdemo1.model.reponse.structure.StudentInformationResponse;
import com.example.capdemo1.model.request.ParentRequest;
import com.example.capdemo1.repository.ParentRepository;
import com.example.capdemo1.repository.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ParentService {

    @Autowired
    public ParentRepository parentRepository;

    @Autowired
    public StudentRepository studentRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(ParentService.class);

    public StudentForParentResponse getParentInformation(String parentId) {
        LOGGER.info("START getParentInformation - ParentService");

        StudentForParentResponse listParentResponse = new StudentForParentResponse();
        try {
            ParentEntity parentEntity = parentRepository.findFirstByParentId(parentId);
            listParentResponse.setParentId(parentEntity.getParentId());
            listParentResponse.setParentFullName(parentEntity.getParentFullName());
            listParentResponse.setAddress(parentEntity.getAddress());
            listParentResponse.setPhone(String.valueOf(parentEntity.getPhone()));
            List<StudentEntity> listStudentEntity = studentRepository.findAllByParentId(parentId);
            List<StudentInformationResponse> studentInformationResponseList = new ArrayList<>();
            listStudentEntity.forEach(studentEntity -> {
                StudentInformationResponse studentInformationResponse = new StudentInformationResponse();
                studentInformationResponse.setStudentId(studentEntity.getStudentId());
                studentInformationResponse.setStudentName(studentEntity.getStudentFullName());
                studentInformationResponse.setPhone(String.valueOf(studentEntity.getPhone()));
                studentInformationResponseList.add(studentInformationResponse);
            });
            listParentResponse.setListStudent(studentInformationResponseList);

        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END getParentInformation - ParentService");
        return listParentResponse;
    }


    public void updateParentInformation(String parentId, ParentRequest parentRequest) throws ParseException {
        LOGGER.info("START updateParentInformation - ParentService");

        ParentEntity parentEntity = parentRepository.findFirstByParentId(parentId);
        try {
            parentEntity.setParentFullName(parentRequest.getParentFullName());
            parentEntity.setAddress(parentRequest.getAddress());
            parentEntity.setPass(parentRequest.getPass());
//            long phone = Integer.parseInt(parentRequest.getPhone());
            parentEntity.setPhone(parentRequest.getPhone());
            parentEntity.setAvatar(parentRequest.getAvatar());
            parentRepository.save(parentEntity);
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END updateParentInformation - ParentService");

    }
}
