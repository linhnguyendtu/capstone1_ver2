package com.example.capdemo1.service;

import com.example.capdemo1.common.Enums;
import com.example.capdemo1.model.entity.AnswerEntity;
import com.example.capdemo1.model.entity.QuestionEntity;
import com.example.capdemo1.model.reponse.QuestionResponse;
import com.example.capdemo1.model.reponse.ResponseModel;
import com.example.capdemo1.model.reponse.structure.ListAnswerByQuestionResponse;
import com.example.capdemo1.model.request.AnswerRequest;
import com.example.capdemo1.model.request.QuestionRequest;
import com.example.capdemo1.repository.AnswerRepository;
import com.example.capdemo1.repository.ClassRepository;
import com.example.capdemo1.repository.ParentRepository;
import com.example.capdemo1.repository.QuestionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class QuestionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionService.class);

    @Autowired
    public QuestionRepository questionRepository;

    @Autowired
    public ParentRepository parentRepository;

    @Autowired
    public ClassRepository classRepository;

    @Autowired
    public AnswerRepository answerRepository;

    private static final int QUESTION_ANSWERED = 1;


    public void postQuestion(QuestionRequest questionRequest) {
        LOGGER.info("START updateParentInformation - QuestionService");
        QuestionEntity questionEntity = new QuestionEntity();
        try {
            questionEntity.setParentId(questionRequest.getParentId());
            questionEntity.setClassId(questionRequest.getClassId());
            questionEntity.setQuestionContent(questionRequest.getQuestionContent());
            questionRepository.save(questionEntity);
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END updateParentInformation - QuestionService");
    }

    public ResponseModel<?> answerQuestion(QuestionRequest questionRequest) {
        LOGGER.info("START answerQuestion - QuestionService");
        ResponseModel<?> responseModel = new ResponseModel<>();
        QuestionEntity questionEntity = questionRepository.findFirstByQuestionId(questionRequest.getQuestionId());
        try {
            if (questionEntity == null) {
                responseModel.setStatus(Enums.Status.ERROR.getValue());
                responseModel.setMessage("Câu hỏi không tồn tại");
                return responseModel;
            }

//            if (questionEntity.getStatus() == QUESTION_ANSWERED) {
//                responseModel.setStatus(Enums.Status.ERROR.getValue());
//                responseModel.setMessage("Đã trả lời câu hỏi này rồi.");
//                return responseModel;
//            }
            AnswerEntity answerEntity = new AnswerEntity();
            questionEntity.setStatus(QUESTION_ANSWERED);
            answerEntity.setAnswerContent(questionRequest.getAnswerContent());
            answerEntity.setQuestionId(questionRequest.getQuestionId());
            answerEntity.setParentId(questionRequest.getParentId());
            answerEntity.setClassId(questionRequest.getClassId());
            answerRepository.save(answerEntity);
            questionRepository.save(questionEntity);
            responseModel.setStatus(Enums.Status.SUCCESS.getValue());
            responseModel.setMessage("Thành công");
            LOGGER.info("END answerQuestion - QuestionService");
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        return responseModel;
    }

    public ResponseModel<?> postAnswerQuestion(AnswerRequest answerRequest) {
        LOGGER.info("START postAnswerQuestion - QuestionService");
        ResponseModel<?> responseModel = new ResponseModel<>();
        AnswerEntity answerEntity = new AnswerEntity();
        try {
            answerEntity.setParentId(answerRequest.getParentId());
            answerEntity.setClassId(answerRequest.getClassId());
            answerEntity.setQuestionId(answerRequest.getQuestionId());
            answerEntity.setAnswerContent(answerRequest.getAnswerContent());
            answerRepository.save(answerEntity);
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        return responseModel;
    }

    public List<QuestionResponse> getQuestionByParentId(String parentId) {
        LOGGER.info("START postAnswerQuestion - QuestionService");
        List<QuestionResponse> questionResponses = new ArrayList<>();
        try {
            List<QuestionEntity> questionEntities = questionRepository.findAllByParentId(parentId);
            questionEntities.forEach(question -> {
                QuestionResponse questionResponse = new QuestionResponse();
                questionResponse.setClassId(question.getClassId());
                questionResponse.setQuestionId(String.valueOf(question.getQuestionId()));
                questionResponse.setQuestionContent(question.getQuestionContent());
                List<AnswerEntity> answerEntities = answerRepository.findAllByQuestionId(question.getQuestionId());
                List<ListAnswerByQuestionResponse> listAnswerByQuestionResponseList = new ArrayList<>();
                answerEntities.forEach(data -> {
                    ListAnswerByQuestionResponse answerByQuestionResponse = new ListAnswerByQuestionResponse();
                    answerByQuestionResponse.setAnswerContent(data.getAnswerContent());
                    answerByQuestionResponse.setCreateDate(String.valueOf(data.getCreateDate()));
                    listAnswerByQuestionResponseList.add(answerByQuestionResponse);
                });
                questionResponse.setListAnswer(listAnswerByQuestionResponseList);
                questionResponses.add(questionResponse);
            });
        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("END getQuestion - QuestionService");
        return questionResponses;

    }

    public ResponseModel<?> putAnswerQuestion(QuestionRequest questionRequest,String questionId) {
        LOGGER.info("START putAnswerQuestion - QuestionService");
        ResponseModel<?> responseModel = new ResponseModel<>();
        QuestionEntity questionEntity = questionRepository.findFirstByQuestionId(Long.valueOf(questionId));
        try {
            if (questionEntity.getQuestionId() == QUESTION_ANSWERED) {
                responseModel.setStatus(Enums.Status.ERROR.getValue());
                responseModel.setMessage("Không thể sửa câu hỏi");
                return responseModel;
            }
            questionEntity.setQuestionContent(questionRequest.getQuestionContent());
            questionRepository.save(questionEntity);
            responseModel.setStatus(Enums.Status.SUCCESS.getValue());
            responseModel.setMessage("Thành công");
            LOGGER.info("END putAnswerQuestion - QuestionService");
        } catch (Exception e) {
            LOGGER.error("ERROR", e);
        }
        LOGGER.info("END putAnswerQuestion - QuestionService");
        return responseModel;
    }

    public ResponseModel<?> deleteQuestion(long questionId) {
        LOGGER.info("START deleteQuestion - QuestionService");
        ResponseModel<?> responseModel = new ResponseModel<>();
        QuestionEntity questionEntity = questionRepository.findFirstByQuestionId(questionId);
        try{
            if (questionEntity == null) {
                responseModel.setStatus(Enums.Status.ERROR.getValue());
                responseModel.setMessage("Câu hỏi không tồn tại");
                return responseModel;
            }
            if (questionEntity.getStatus() == QUESTION_ANSWERED) {
                answerRepository.deleteAllByQuestionId(questionId);
            }
            questionRepository.deleteById(questionId);


            responseModel.setStatus(Enums.Status.SUCCESS.getValue());
            responseModel.setMessage("Thành công");
            LOGGER.info("END deleteQuestion - QuestionService");
        }
        catch (Exception e){
            LOGGER.error("ERROR", e);
        }
        LOGGER.info("END deleteQuestion - QuestionService");
        return responseModel;

    }
}
