package com.example.capdemo1.service;

import com.example.capdemo1.model.entity.CategoryEntity;
import com.example.capdemo1.model.entity.NewEntity;
import com.example.capdemo1.model.reponse.NewResponse;
import com.example.capdemo1.model.request.NewRequest;
import com.example.capdemo1.repository.CategoryRepository;
import com.example.capdemo1.repository.NewRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NewService {
    @Autowired
    public NewRepository newRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ModelMapper modelMapper;

    public static final int RECORD_PER_PAGE = 5;

    public void saveNew(NewRequest request) {

        NewEntity newEntity = modelMapper.map(request, NewEntity.class);
        CategoryEntity category = categoryRepository.findFirstByCode(request.getCategory());
        newEntity.setCategory(category);
        newRepository.save(newEntity);
    }

    public List<NewResponse> getAllNew() {
        List<NewEntity> newEntityList = newRepository.findAll();
        return newEntityList
                .stream()
                .map(entity -> mappingResponse(entity))
                .collect(Collectors.toList());
    }

    public List<NewResponse> getNewByCategory(Long CategoryId) {
        Pageable firstPageWithTwoElements = PageRequest.of(0, 10);
        List<NewEntity> newEntity = newRepository.findAllByCategoryId(CategoryId, firstPageWithTwoElements);
        return newEntity
                .stream()
                .map(entity -> modelMapper.map(entity, NewResponse.class))
                .collect(Collectors.toList());
    }

    private NewResponse mappingResponse(NewEntity entity) {
        NewResponse response = modelMapper.map(entity, NewResponse.class);
        return response;
    }

    public NewResponse getNewDetail(Long id) {
        Optional<NewEntity> newEntity = newRepository.findFirstById(id);
        return newEntity
                .stream()
                .map(entity -> modelMapper.map(entity, NewResponse.class))
                .collect(Collectors.toList()).get(0);
    }

    public void deleteNew(Long id) {
        newRepository.deleteById(id);
    }
    public void updateNew(Long id, NewRequest newRequest) {
        //Optinal chống null
        Optional<NewEntity> oldNewEntity = newRepository.findFirstById(id);
        if(oldNewEntity.isPresent()){
            NewEntity newEntity= modelMapper.map(newRequest, NewEntity.class);
            newEntity.setId(id);
            CategoryEntity category = categoryRepository.findFirstByCode(newRequest.getCategory());
            newEntity.setCategory(category);
            newRepository.save(newEntity);

        }
    }
}





