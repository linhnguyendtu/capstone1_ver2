package com.example.capdemo1.service;

import com.example.capdemo1.common.Enums;
import com.example.capdemo1.model.entity.*;
import com.example.capdemo1.model.reponse.FinanceResponse;
import com.example.capdemo1.repository.*;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FinanceService {
    @Autowired
    public ParentRepository parentRepository;

    @Autowired
    public StudentRepository studentRepository;

    @Autowired
    public CourseRepository courseRepository;

    @Autowired
    public FinanceRepository financeRepository;

    @Autowired
    public ClassRepository classRepository;

    @Autowired
    public SchoolRepository schoolRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(FinanceService.class);

    public List<FinanceResponse> getFinanceByParentId(String parentId) {
        LOGGER.info("START getFinanceByParentId - FinanceService");
        List<FinanceResponse> listFinanceResponses = new ArrayList<>();
        try {
            StudentEntity studentEntity = studentRepository.findFirstByParentId(parentId);
            ClassEntity classEntity = classRepository.findFirstByStudentId(studentEntity.getStudentId());
            SchoolEntity schoolEntity = schoolRepository.findFirstByClassId(classEntity.getClassId());
            List<FinanceEntity> financeEntity = financeRepository.findAllByStudentId(studentEntity.getStudentId());
            financeEntity.forEach(data -> {
                CourseEntity courseEntity = courseRepository.findFirstByCourseId(data.getCourseId());
                FinanceResponse financeResponse = new FinanceResponse();
                financeResponse.setStudentId(data.getStudentId());
                financeResponse.setStudentFullName(studentEntity.getStudentFullName());
                financeResponse.setClassFullName(classEntity.getClassFullName());
                financeResponse.setSchoolName(schoolEntity.getSchoolName());
                financeResponse.setAcademyYear(courseEntity.getAcademyYear());
                financeResponse.setSemester(Enums.Semester.findByCode(courseEntity.getSemester()).getValue());
                financeResponse.setFinanceName(data.getFinanceName());
                financeResponse.setMoney(data.getMoney());
                financeResponse.setMoneyType(data.getMoneyType());
                financeResponse.setStatus(Enums.FinanceStatus.findByCode(data.getStatus()).getValue());
                financeResponse.setCreateDate(data.getCreateDate().toString());
                listFinanceResponses.add(financeResponse);
            });

        } catch (Exception e) {
            LOGGER.error("ERROR:", e);
        }
        LOGGER.info("STAR getFinanceByParentId - FinanceService");
        return listFinanceResponses;
    }

}





