package com.example.capdemo1.common;

public class Enums {
    public enum SubjectStatus {
        DOING(0, "Đang học"),
        DONE(1, "Hoàn Thành"),
        FAIL(2, "Không đạt yêu cầu");
        private int code;
        private String value;

        public int getCode() {
            return code;
        }
        public String getValue() {
            return value;
        }

        SubjectStatus(int code, String value) {
            this.code = code;
            this.value = value;
        }
        public static SubjectStatus findByCode(int code) {
            for (SubjectStatus subjectStatus : values()) {
                if (subjectStatus.getCode() == code) {
                    return subjectStatus;
                }
            }
            return null;
        }
    }
    public enum FinanceStatus {
        DOING(0, "Chưa Thanh Toán"),
        DONE(1, "Đã Thanh Toán");
        private int code;
        private String value;

        public int getCode() {
            return code;
        }
        public String getValue() {
            return value;
        }

        FinanceStatus(int code, String value) {
            this.code = code;
            this.value = value;
        }
        public static FinanceStatus findByCode(int code) {
            for (FinanceStatus financeStatus : values()) {
                if (financeStatus.getCode() == code) {
                    return financeStatus;
                }
            }
            return null;
        }
    }
    public enum Semester{
        HK1(1,"Kì 1"),
        HK2(2,"Kì 2"),
        SUM(3,"Học kì Hè");
        private int code;
        private String value;

        Semester(int code, String value) {
            this.code = code;
            this.value = value;
        }

        public int getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }
        public static Semester findByCode(int code){
            for (Semester semester : values()){
                if(semester.getCode() == code){
                    return semester;
                }
            }
            return null;
        }
    }
    public enum Status{
        ERROR(1,"ERROR"),
        SUCCESS(2,"SUCCESS");

        private int code;

        public int getCode() {
            return code;
        }

        public String getValue() {
            return value;
        }

        private String value;

        Status(int code, String value) {
            this.code = code;
            this.value = value;
        }
    }
}
