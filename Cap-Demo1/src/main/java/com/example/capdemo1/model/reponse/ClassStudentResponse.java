package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.ListClassOfTeacher;
import com.example.capdemo1.model.reponse.structure.StudentOfClassReponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassStudentResponse {

    @JsonProperty("class_id")
    private String classId;

    @JsonProperty("class_name")
    private String classFullName;

    @JsonProperty("number_of_member")
    private String numberOfMember;

    @JsonProperty("list_student_of_class")
    private List<StudentOfClassReponse> listStudent;



}
