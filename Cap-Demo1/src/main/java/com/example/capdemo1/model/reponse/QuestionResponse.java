package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.ListAnswerByQuestionResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {

    @JsonProperty("class_id")
    private String classId;

    @JsonProperty("question_id")
    private String questionId;

    @JsonProperty("question_content")
    private String questionContent;

    @JsonProperty("create_date")
    private Date createDate;

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("list_answer")
    private List<ListAnswerByQuestionResponse> listAnswer;
}
