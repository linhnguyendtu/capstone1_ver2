package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "student")
public class StudentEntity {
    @Id
    private String studentId;

    @Column(name = "parent_id")
    private String parentId;

    @Column(name = "class_id")
    private String classId;

    @Column(name = "course_id")
    private String courseId;

    @Column(name = "student_full_name")
    private String studentFullName;

    @Column(name = "gender")
    private int gender;

    @Column(name = "avatar")
    private String avatar;

    @CreationTimestamp
    @Temporal(TemporalType.DATE)
    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private int phone;

    @Column(name = "address")
    private String address;

    @Column(name = "join_date")
    private Date joinDate;

    @Column(name = "role")
    private int role;

    @Column(name = "create_at")
    private Date createAt;



}
