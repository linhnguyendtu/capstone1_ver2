package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "teacher")
public class TeacherEntity {
    @Id
    private String teacherId;

    @Column(name = "teacher_full_name")
    private String teacherFullName;

    @Column(name = "gender")
    private int gender;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private Long phone;

    @Column(name = "account")
    private String account;

    @Column(name = "pass")
    private String pass;

    @Column(name = "address")
    private String address;

    @Column(name="avatar")
    private String avatar;

    @Column(name = "role")
    private int role;

    @Column(name = "working_day")
    private int workingDay;

    @Column(name = "create_at")
    private Date createAt;

}
