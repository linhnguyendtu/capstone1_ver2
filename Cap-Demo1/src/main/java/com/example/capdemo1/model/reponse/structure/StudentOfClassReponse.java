package com.example.capdemo1.model.reponse.structure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentOfClassReponse {

    @JsonProperty("student_id")
    private String studentId;

    @JsonProperty("student_name")
    private String studentName;

    @JsonProperty("student_birthday")
    private String studentBirthday;

    @JsonProperty("student_email")
    private String studentEmail;

    @JsonProperty("student_address")
    private String studentAddress;

    @JsonProperty("phone")
    private String phone;

}
