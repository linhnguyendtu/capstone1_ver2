package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.ListClassOfTeacher;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassResponse {

    @JsonProperty("teacher_id")
    private String teacherId;

    @JsonProperty("teacher_name")
    private String teacherName;

    @JsonProperty("teacher_avatar")
    private String avatar;

    @JsonProperty("teacher_birthday")
    private String teacherBirthday;

    @JsonProperty("email")
    private String email;

    @JsonProperty("list_class_of_teacher")
    private List<ListClassOfTeacher> listClass;


}
