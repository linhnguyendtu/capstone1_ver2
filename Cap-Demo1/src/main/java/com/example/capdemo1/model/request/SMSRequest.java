package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SMSRequest {
    @JsonProperty("ApiKey")
    private String ApiKey ="02196988D88080A0127D3DBDE633BA" ;

    @JsonProperty("SecretKey")
    private String SecretKey = "02196988D88080A0127D3DBDE633BA";

    @JsonProperty("Content")
    private String Content ;

    @JsonProperty("Phone")
    private String Phone  ;

    @JsonProperty("SmsType")
    private Integer SmsType=8 ;

}
