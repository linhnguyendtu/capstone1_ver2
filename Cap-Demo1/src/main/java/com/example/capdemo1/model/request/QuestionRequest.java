package com.example.capdemo1.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionRequest {

    @JsonProperty("question_id")
    private long questionId;

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("class_id")
    private String classId;

    @JsonProperty("question_content")
    private String questionContent;

    @JsonProperty("answer_content")
    private String answerContent;

}
