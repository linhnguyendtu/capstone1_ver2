package com.example.capdemo1.model.reponse.structure;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListAnswerByQuestionResponse {

    @JsonProperty("answer_content")
    private String answerContent;

    @JsonProperty("create_date")
    private String createDate;
}
