package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.ListClassOfTeacher;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneResponse {

    @JsonProperty("phone")
    private String phone;

}
