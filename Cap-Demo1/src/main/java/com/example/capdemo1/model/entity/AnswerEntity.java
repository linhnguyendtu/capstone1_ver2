package com.example.capdemo1.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "answer")
public class AnswerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long answerId;

    @Column(name="question_id")
    private long questionId;

    @Column(name="parent_id")
    private String parentId;

    @Column(name = "class_id")
    private String classId;

    @Column(name = "answer_content")
    private String answerContent;

    @CreationTimestamp
    @Temporal(TemporalType.DATE)
    @Column(name = "creat_date")
    private Date createDate;

}
