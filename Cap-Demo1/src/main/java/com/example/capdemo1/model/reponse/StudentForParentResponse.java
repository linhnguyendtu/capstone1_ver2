package com.example.capdemo1.model.reponse;

import com.example.capdemo1.model.reponse.structure.StudentInformationResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentForParentResponse {

    @JsonProperty("parent_id")
    private String parentId;

    @JsonProperty("parent_full_name")
    private String parentFullName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("address")
    private String address;

    @JsonProperty("list_student")
    private List<StudentInformationResponse> listStudent;

}
