package com.example.capdemo1.model.login;

public enum ERole {
    ROLE_PARENT,
    ROLE_TEACHER,
    ROLE_ADMIN
}
