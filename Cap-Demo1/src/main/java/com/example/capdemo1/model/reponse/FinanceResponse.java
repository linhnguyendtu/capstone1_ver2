package com.example.capdemo1.model.reponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinanceResponse {

    @JsonProperty("student_id")
    private String studentId;

    @JsonProperty("student_full_name")
    private String studentFullName;

    @JsonProperty("class_full_name")
    private String classFullName;

    @JsonProperty("school_name")
    private String schoolName;

    @JsonProperty("academy_year")
    private String academyYear;

    @JsonProperty("semester")
    private String semester;

    @JsonProperty("finance_name")
    private String financeName;

    @JsonProperty("money")
    private String money;

    @JsonProperty("money_type")
    private String moneyType;

    @JsonProperty("status")
    private String status;

    @JsonProperty("create_date")
    private String createDate;

}
