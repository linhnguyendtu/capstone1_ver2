import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
const API_URL = '';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  // This service provides methods to access public and protected resources
  constructor(private http: HttpClient) { }
  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }
  getParentBoard(): Observable<any> {
    return this.http.get(API_URL + 'parent', { responseType: 'text' });
  }
  getSchoolBoard(): Observable<any> {
    return this.http.get(API_URL + 'school', { responseType: 'text' });
  }
  getTeacherBoard(): Observable<any> {
    return this.http.get(API_URL + 'teacher', { responseType: 'text' });
  }
}
