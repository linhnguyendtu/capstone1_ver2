import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { News } from 'src/app/models/news/news.model';




@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private apiUrl='http://localhost:8080/api/news/all-new';
  private getNewsDetailsUrl='http://localhost:8080/api/news';
  private apiGetAboutDTU='http://localhost:8080/api/news/category-id/3';
  constructor(private httpClient: HttpClient ) { }

  getAllNews():Observable<News[]>{
    return this.httpClient.get<News[]>(`${this.apiUrl}`);
  }
  getNewsDetailById(id:number): Observable<News>{
    return this.httpClient.get<News>(`${this.getNewsDetailsUrl}/${id}`)
  }
  getAboutDTU():Observable<News[]>{
    return this.httpClient.get<News[]>(`${this.apiGetAboutDTU}`);
  }

}
