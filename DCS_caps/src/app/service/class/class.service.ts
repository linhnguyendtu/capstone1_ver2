import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Class} from "../../models/class/class.model";
import {Students} from "../../models/students/students.model";
import {Question} from "../../models/question/question.model";


@Injectable({
  providedIn: 'root'
})
export class ClassService {
  private apiUrlGetClassByTeacherId="http://localhost:8080/api/teacher/T01";
  private apiUrlGetStudentByClassId="http://localhost:8080/api/teacher/class";
  private apiGetQuestion="http://localhost:8080/api/teacher/question";
  private apiPostAnswer="http://localhost:8080/api/question/answer-question";
  id: any;
  Student: any;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient: HttpClient) {}

  getClassByTeacherId():Observable<Class[]>{
    return this.httpClient.get<Class[]>(`${this.apiUrlGetClassByTeacherId}`);
  }

  getStudentByClassId(id: any):Observable<Students[]>{
    return this.httpClient.get<Students[]>(`${this.apiUrlGetStudentByClassId}/${id}`);
  }
  getQuestionByClassId(id: any): Observable<Question[]> {
    return this.httpClient.get<Question[]>(`${this.apiGetQuestion}/${id}`);
  }
  updateAnswer(id: any, content: any): Observable<any> {
    return this.httpClient.put(this.apiPostAnswer + '/' + id, content)
  }
}
