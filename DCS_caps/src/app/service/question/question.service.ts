import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Question } from 'src/app/models/question/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {
  getQuestionUrl: string = 'http://localhost:8080/api/question/my-question/P01';
  postQuestionUrl: string = 'http://localhost:8080/api/question/create-question';
  question: Question;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) { }

  getQuestion(): Observable<any> {
    return this.http.get(this.getQuestionUrl);
  }

  addQuestion(question: Question): Observable<any> {
    const body=JSON.stringify(question);
    return this.http.post(this.postQuestionUrl, body, this.httpOptions);
  }

  updateQuestion(id: any, content: any): Observable<any> {
    return this.http.put(this.getQuestionUrl + '/' + id, content)
  }

  deleteQuestion(id: any): Observable<any> {
    return this.http.delete(this.getQuestionUrl + '/' +id);
  }
}
