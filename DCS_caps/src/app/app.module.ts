import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageParentComponent } from './layout/parent/homepage-parent/homepage-parent/homepage-parent.component';
import { NavbarComponent } from './layout/parent/nav-bar/navbar/navbar.component';
import { StudyComponent } from './layout/parent/study/study/study.component';
import { StudyService } from './service/study/study.service';
import { BannerComponent } from './layout/parent/banner/banner/banner.component';
import { NotificationComponent } from './layout/parent/notification/notification/notification.component';
import { TuitionComponent } from './layout/parent/tuition/tuition-dashboard/tuition/tuition.component';
import { TuitionDetailComponent } from './layout/parent/tuition/tuition-detail/tuition-detail/tuition-detail.component';
import { ContactComponent } from './layout/parent/contact/contact-dashboard/contact/contact.component';
import { ApartmentComponent } from './layout/parent/contact/apartment/apartment/apartment.component';
import { AccountComponent } from './layout/parent/account/account/account.component';
import { NewsEventListComponent } from './layout/parent/home-content/news-event-list/news-event-list.component';
import { HomeComponent } from './layout/parent/home/home/home.component';
import { DtuContentListComponent } from './layout/parent/home-content/dtu-content-list/dtu-content-list.component';
import { QuestionDashboardComponent } from './layout/parent/Question/Question-dashboard/question-dashboard/question-dashboard.component';
import { ParentQuestionComponent } from './layout/parent/Question/parent-question/parent-question/parent-question.component';
import { LoginComponent } from './layout/login/login.component';
import { HomeTeacherComponent } from './layout/teacher/home-teacher/home-teacher.component';
import { HeaderComponent } from './layout/teacher/header/header.component';
import { FooterComponent } from './layout/teacher/footer/footer.component';
import { ClassComponent } from './layout/teacher/class/class.component';
import { SlidebarComponent } from './layout/teacher/slidebar/slidebar.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatMenuModule} from "@angular/material/menu";
import {MatListModule} from "@angular/material/list";
import {RouterModule} from "@angular/router";
import {MatTabsModule} from "@angular/material/tabs";
import {MatFormFieldModule} from "@angular/material/form-field";

import { ToastrModule } from 'ngx-toastr';
import {MatBadgeModule} from '@angular/material/badge';


@NgModule({
  declarations: [
    AppComponent,
    HomepageParentComponent,
    NavbarComponent,
    StudyComponent,
    BannerComponent,
    NotificationComponent,
    TuitionComponent,
    TuitionDetailComponent,
    ContactComponent,
    ApartmentComponent,
    AccountComponent,
    NewsEventListComponent,
    HomeComponent,
    DtuContentListComponent,
    QuestionDashboardComponent,
    ParentQuestionComponent,
    LoginComponent,
    HomeTeacherComponent,
    HeaderComponent,
    FooterComponent,
    ClassComponent,
    SlidebarComponent

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        CarouselModule,
        MatSidenavModule,
        MatDividerModule,
        MatToolbarModule,
        MatIconModule,
        FlexLayoutModule,
        MatMenuModule,
        MatListModule,
        RouterModule,
        MatTabsModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        ToastrModule.forRoot({
          timeOut: 3000,
          progressBar: true,
          progressAnimation: "increasing"
        }),
        MatBadgeModule
    ],
  providers: [StudyService],
  bootstrap: [AppComponent]
})
export class AppModule {}

