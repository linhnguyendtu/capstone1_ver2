import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ClassService} from "../../../service/class/class.service";
import {Class} from "../../../models/class/class.model";
import { Router } from '@angular/router';

@Component({
  selector: 'app-slidebar',
  templateUrl: './slidebar.component.html',
  styleUrls: ['./slidebar.component.css']
})
export class SlidebarComponent implements OnInit {
  @Output() buttonClicked : EventEmitter<string> = new EventEmitter<string>();
  Class: Class[];
  teacher: any;
  id: any;
  classOfTeacher: Array<string> = [];
  constructor(private classService: ClassService,
              private router:Router) { }

  ngOnInit(): void {
    this.getClassByTeacherId();
  }
  private getClassByTeacherId(){
    this.classService.getClassByTeacherId().subscribe(data=>{
      this.Class=data;
      this.teacher = this.Class[0];
      for(let listClass of this.teacher.list_class_of_teacher) {
        this.classOfTeacher.push(listClass.class_id);
      }
    })
  }

  onClickGetClassId(classId: string){
    this.id =classId;
    this.buttonClicked.emit(classId);
  }
}
