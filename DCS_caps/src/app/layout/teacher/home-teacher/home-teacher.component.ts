import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Component({
  selector: 'app-home-teacher',
  templateUrl: './home-teacher.component.html',
  styleUrls: ['./home-teacher.component.css']
})
export class HomeTeacherComponent implements OnInit {
  classId: string;
  sideBarOpen=true;
  public classId$ = new BehaviorSubject<string>("ClassId");
  constructor() { }

  ngOnInit(): void {}

  sideBarToggler($event: any) {
    this.sideBarOpen = !this.sideBarOpen;
  }
  onClickGetClassId(event : string){
    this.classId = event;
    localStorage.setItem('classId', event);
    this.classId$.next(event);

    // console.log("Event: "+event);
    // console.log("classId:"+this.classId)
    // console.log("classId$"+this.classId$);
  }




}
