import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ClassService} from "../../../service/class/class.service";
import {Students} from "../../../models/students/students.model";
import {ActivatedRoute} from "@angular/router";
import {HomeTeacherComponent} from "../home-teacher/home-teacher.component";
import {Sms} from "../../../models/sms/sms.model";
import {SmsService} from "../../../service/sms/sms.service";
import {NgForm} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {ParentService} from "../../../service/parent/parent.service";
import {Question} from "../../../models/question/question.model";

@Component({
  selector: 'app-class',
  templateUrl: './class.component.html',
  styleUrls: ['./class.component.css']
})
export class ClassComponent implements OnInit, OnChanges {
  Student: any;
  id: any;
  questionId: any;
  listStudent = [];
  listQuestion: any = [];
  sms: Sms = new Sms();
  content: string;
  answerContent: string;
  parentId: any;
  isClickedOpenForm: boolean = false;
  list_answer = new Array();

  constructor(private classService: ClassService,
              private router: ActivatedRoute,
              private home: HomeTeacherComponent,
              private smsService: SmsService,
              private toastService: ToastrService,
              private parentService: ParentService
  ) {
  }

  ngOnInit(): void {
    this.Student = new Students();
    if (localStorage.getItem('classId')) {
      this.home.classId$.subscribe((classId) => {
        this.id = classId;
        this.classService.getStudentByClassId(localStorage.getItem('classId')).subscribe(data => {
          this.Student = data[0];
          console.log(this.Student);
        })
      })
    }
    this.getQuestion();
    this.parentService.getParent().subscribe((res: any) => this.parentId = res.parent_id);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }

  openForm() {
    this.isClickedOpenForm = !this.isClickedOpenForm;
  }

  getID(question: Question) {
    this.questionId = question.question_id;
  }

  // Gửi tin nhắn
  onClickSubmit(smsForm: NgForm) {

    this.sms.ApiKey = "02196988D88080A0127D3DBDE633BA";
    this.sms.SecretKey = "0E86D8433CC9289BEFD0CA3EA27492";
    this.sms.SmsType = "8";
    this.sms.Content = this.content;

    this.home.classId$.subscribe((classId) => {
      this.id = localStorage.getItem('classId');
      console.log(this.id);
      console.log(this.sms);
      this.smsService.postSendSms(this.id, this.sms).subscribe({
        next: data => {
          this.toastService.success("Gửi tin nhắn thành công!!", "Notification");
          smsForm.reset();
        },
        error: error => {
          this.toastService.error("Gửi tin nhắn không thành công!!", "Warning!!!");
        }
      });
    });
  }

  // Lấy câu hỏi hiện thị lên UI
  getQuestion() {
    this.home.classId$.subscribe((classId) => {
      this.id = localStorage.getItem('classId');
      console.log(this.id);
      this.classService.getQuestionByClassId(this.id).subscribe({
        next: data => {
          this.listQuestion = data;
          this.list_answer.length = this.listQuestion.length;
          this.list_answer = this.list_answer.fill('');
          console.log("danh sách cau hỏi: ", this.listQuestion);
        },
        error: error => {
          alert("Lỗi rồi lại đi bạn ơi!!!")
        }
      });
    });
  }

  //Gửi câu trả lời
  sendAnswer(questionForm: NgForm) {
    let answer = this.answerContent;
    let classID = this.id;
    let parrentID;
    let i=0;
    answer = this.list_answer[i];
    let body = {
      parent_id: this.parentId,
      class_id: classID,
      answer_content: answer
    }
    this.classService.updateAnswer(this.questionId, body).subscribe({
      next: data => {
        this.toastService.success("Trả lời câu hỏi thành công", "Notification");
        this.getQuestion();
      },
      error: error => {
        this.toastService.error("Trả lời lỗi", "ERROR")
      }
    });
  }
}
