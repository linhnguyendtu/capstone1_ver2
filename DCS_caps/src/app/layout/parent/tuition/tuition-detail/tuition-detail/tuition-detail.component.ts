import { Component, OnInit } from '@angular/core';
import { TuitionService } from 'src/app/service/tuition/tuition.service';

@Component({
  selector: 'app-tuition-detail',
  templateUrl: './tuition-detail.component.html',
  styleUrls: ['./tuition-detail.component.css']
})
export class TuitionDetailComponent implements OnInit {

  public firstYearFirstSemester: Array<any> = [];
  public firstYearSecondSemester: Array<any> = [];
  public firstYearSummerSemester: Array<any> = [];

  public secondYearFirstSemester: Array<any> = [];
  public secondYearSecondSemester: Array<any> = [];
  public secondYearSummerSemester: Array<any> = [];

  public thirdYearFirstSemester: Array<any> = [];
  public thirdYearSecondSemester: Array<any> = [];
  public thirdYearSummerSemester: Array<any> = [];

  public fourthYearFirstSemester: Array<any> = [];
  public fourthYearSecondSemester: Array<any> = [];
  public fourthYearSummerSemester: Array<any> = [];

  constructor(private TuitionService: TuitionService) { }

  ngOnInit(): void {
    this.getTuitionDetail();
  }

  getTuitionDetail() {
    this.TuitionService.getTuition().subscribe((res: any) => {
      console.log("Tuitiion ver2.2: ", res);
      let check = true;
      for(let subject of res) {
        console.log("test");
        if(subject.semester === "Kì 1") {
          console.log('ok');
          if(subject.academy_year === "2018-2019") this.firstYearFirstSemester.push(subject);
          else if(subject.academy_year === "2019-2020") this.secondYearFirstSemester.push(subject);
          else if(subject.academy_year === "2020-2021") this.thirdYearFirstSemester.push(subject);
          else this.fourthYearFirstSemester.push(subject);
        }
        else if(subject.semester === "Kì 2") {
          if(subject.academy_year === "2018-2019") this.firstYearSecondSemester.push(subject);
          else if(subject.academy_year === "2019-2020") this.secondYearSecondSemester.push(subject);
          else if(subject.academy_year === "2020-2021") this.thirdYearSecondSemester.push(subject);
          else this.fourthYearSecondSemester.push(subject);
        }
        else if(subject.semester === "Học kì hè") {
          if(subject.academy_year === "2018-2019") this.firstYearSummerSemester.push(subject);
          else if(subject.academy_year === "2019-2020") this.secondYearSummerSemester.push(subject);
          else if(subject.academy_year === "2020-2021") this.thirdYearSummerSemester.push(subject);
          else this.fourthYearSummerSemester.push(subject);
        }
      }
    })
  }
}
