import { Component, OnInit } from '@angular/core';
import { ParentService } from 'src/app/service/parent/parent.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  parent: any;
  constructor(private parentService: ParentService) { }

  ngOnInit(): void {
    this.parentService.getParent().subscribe((res:any) => {
      this.parent = res;
      console.log('đây là parent: ',this.parent);
    })
  }
}
