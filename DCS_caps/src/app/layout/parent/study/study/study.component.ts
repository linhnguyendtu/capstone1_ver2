import { Component, OnInit } from '@angular/core';

import { StudyService } from 'src/app/service/study/study.service';
import { Subject } from 'src/app/models/subject/subject.model';


@Component({
  selector: 'app-study',
  templateUrl: './study.component.html',
  styleUrls: ['./study.component.css']
})
export class StudyComponent implements OnInit {
  public courses: Array<any> = [];
  public subjects: Array<any> = [];
  public gpa: string;
  public selectedValue: string = '';
  studentsName: Array<string> = [];

  constructor(private StudyService: StudyService) { }

  ngOnInit(): void {
    this.getGPA();
    this.getStudentName();
    this.getScoreByStudentName(this.selectedValue);
  }
  onChange(nameStudent: string) {
    console.log('nameStudent: ', nameStudent);
    this.selectedValue = nameStudent;
    this.getScoreByStudentName(this.selectedValue);
  }
  getGPA() {
    this.StudyService.getSubjects().subscribe((res:any) => {
      let sumScore = 0;
      let numberOfSubject = 0;
        for(let subject of res) {
          // Môn học chưa học có điểm = 0
          if(subject.out_of_10 === "") subject.out_of_10 = 0;
          subject.out_of_10 = parseInt(subject.out_of_10);
          sumScore += subject.out_of_10;
          numberOfSubject++;
        }
        this.gpa = (sumScore / numberOfSubject).toFixed(1);
    });
    return this.gpa;
  }
  getStudentName() {
    this.StudyService.getSubjects().subscribe((res: any) => {
      for(let student of res) {
        this.studentsName.push(student.student_full_name);        
      }
      this.selectedValue = this.studentsName[0];
      this.getScoreByStudentName(this.selectedValue);
    })
  }
  getScoreByStudentName(studentName: string) {
    this.StudyService.getSubjects().subscribe((res: any) => {
      for(let student of res) {
        console.log('ten dc chon: ', studentName);
        if(studentName === student.student_full_name) {
          this.courses = student.list_course;
          break;
        }
      }
    })
  }
}
/**
 * [
  {
        "student_id": "ST01",
        "student_full_name": "Nguyễn Văn Linh",
        "list_course": [
            {
                "academy_year": "2018-2019",
                "semester": "Học Kì I",
                "list_subject": [
                    {
                        "subject_name": "Toán cao cấp",
                        "out_of_10": "8",
                        "out_of_4": "3",
                        "status": 1
                    }
                ]
            },
            {
                "academy_year": "2018-2019",
                "semester": "Học Kì II",
                "list_subject": [
                    {
                        "subject_name": "Lập trình cơ sở",
                        "out_of_10": "9",
                        "out_of_4": "4",
                        "status": 1
                    },
                    {
                        "subject_name": "Linh si đa",
                        "out_of_10": "8",
                        "out_of_4": "3",
                        "status": 1
                    }
                ]
            }
        ]
    },
    {
        "student_id": "ST02",
        "student_full_name": "Nguyễn Anh DŨng",
        "list_course": []
    }
]
 */