import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/news/news.model';
import { NewsService } from 'src/app/service/news/news.service';
import { Router } from '@angular/router';

import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-news-event-list',
  templateUrl: './news-event-list.component.html',
  styleUrls: ['./news-event-list.component.css']
})
export class NewsEventListComponent implements OnInit {
  AllNews:News[];
  constructor(private  newsService: NewsService, private routers:Router) { }

  //Slider tin tức
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplaySpeed:2000,
    navSpeed: 700,
    nav: true,
    navText: ['Quay Lại', 'Tiếp Tục'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    
  }

  ngOnInit(): void {
  this.getAllNews();
  }
  private getAllNews() {
    this.newsService.getAllNews().subscribe(data=>{
        this.AllNews=data;
        console.log(this.AllNews);
    })
  }
  newDetails(id:number){
    this.routers.navigate(['new-details',id]);
  }
}



