import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/news/news.model';
import { NewsService } from 'src/app/service/news/news.service';
import { Router } from '@angular/router';

import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-dtu-content-list',
  templateUrl: './dtu-content-list.component.html',
  styleUrls: ['./dtu-content-list.component.css']
})
export class DtuContentListComponent implements OnInit {
  AboutDTU:News[];
  constructor(private  newsService: NewsService, private routers:Router) { }
  ngOnInit(): void {
   this.getAboutDTU();
  }
  private getAboutDTU(){
    this.newsService.getAboutDTU().subscribe(data=>{
      this.AboutDTU=data;
    })
  }


  //Slider tin tức
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    autoplay: true,
    autoplayTimeout: 6000,
    autoplaySpeed:2000,
    navSpeed: 1000,
    navText: ['Quay Lại', 'Tiếp Theo'],
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    
  }
}
