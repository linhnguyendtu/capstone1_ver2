import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DtuContentListComponent } from './dtu-content-list.component';

describe('DtuContentListComponent', () => {
  let component: DtuContentListComponent;
  let fixture: ComponentFixture<DtuContentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DtuContentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DtuContentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
