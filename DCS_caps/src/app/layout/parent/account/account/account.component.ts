import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Parent } from 'src/app/models/parent/parent.model';
import { ParentService } from 'src/app/service/parent/parent.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  isChecked = false;
  isClicked = false;
  isSubmitted = false;
  id: any;
  parent: any;
  imgUrl: any;
  constructor(private ParentService: ParentService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get("this.id");
    this.getParent();
  }
  changePassToggle() {
    this.isChecked = !this.isChecked;
  }
  changePassToggle2() {
    this.isClicked = !this.isClicked;
  }
  getParent() {
    this.ParentService.getParentById(this.id).subscribe((res: any) => {
      this.parent = res;

      this.id = this.parent.id;
    });
  }
  onSubmit(form: NgForm) {
    let student_id = this.parent.list_student[0].student_id;
    let student_full_name = this.parent.list_student[0].student_full_name;
    let parent = {
      id: this.id,
      parent_full_name: form.value.name,
      address: form.value.address,
      phone: form.value.phone,
      student_id: this.parent.student_id,
      student_full_name: this.parent.student_full_name
    }
    console.log('parent: ', parent);
    this.ParentService.updateAccount(this.id, parent).subscribe(
      data => {
        this.isSubmitted = !this.isSubmitted;
        console.log('id:', this.id);
      }
    );
  }
  onSelectFile(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imgUrl = event.target?.result;
      }
    }
    console.log('Đường dẫn ảnh: ', this.imgUrl);
  }
  delete() {
    this.imgUrl = null;
  }
}
