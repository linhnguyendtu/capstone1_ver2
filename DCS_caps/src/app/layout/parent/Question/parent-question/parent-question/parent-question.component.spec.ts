import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentQuestionComponent } from './parent-question.component';

describe('ParentQuestionComponent', () => {
  let component: ParentQuestionComponent;
  let fixture: ComponentFixture<ParentQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParentQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
