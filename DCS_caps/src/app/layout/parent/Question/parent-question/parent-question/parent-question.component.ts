import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Question } from 'src/app/models/question/question.model';
import { ParentService } from 'src/app/service/parent/parent.service';
import { QuestionService } from 'src/app/service/question/question.service';


@Component({
  selector: 'app-parent-question',
  templateUrl: './parent-question.component.html',
  styleUrls: ['./parent-question.component.css']
})
export class ParentQuestionComponent implements OnInit {
  isClicked = false;
  isClose = true;
  isClickedAnswer = false;
  isClickedInput = false;
  id: any;
  question: Question = new Question();
  list_question: any = [];
  questionToPassForm: any;
  // content for update question
  content: string;
  constructor(private questionService: QuestionService, private parentService: ParentService) { }

  ngOnInit(): void {
    this.parentService.getParent().subscribe((res: any) => {
      this.question.parent_id = res.id;
    })
    this.refreshQuestion();
  }
  refreshQuestion() {
    this.questionService.getQuestion().subscribe((res) => {
      this.list_question = res;
      console.log('còn đây là list question của tui: ', this.list_question);
    })
  }
  onSubmit(form: NgForm) {
    this.question.question_content = form.value.content;
    this.question.class_id = "TPM4";
    this.question.parent_id="P01";

    this.questionService.addQuestion(this.question).subscribe({
      next: data => {
        alert("Thêm thành công");
        this.refreshQuestion();
      },
      error: error => {
        alert("Lỗi rồi, làm lại đê");
      }
    });
  }
  openQuestionForm() {
    this.isClicked = !this.isClicked;
  }
  openAnswerView(question: any) {
    this.isClickedInput = !this.isClickedInput;
    this.questionToPassForm = question;
    console.log('questionToPassForm: ', this.questionToPassForm);
  }

  updateQuestion(question: any) {
    console.log('mày là thằng nào hả id: ', question.id);
    const body = {
      class_id: question.class_id,
      question_content: this.content,
       create_date: question.create_date,
      list_question: question.list_question,// cái này là cái gì???
    };
    console.log('tao sẽ update cái qq này: ', body);
    this.questionService.updateQuestion(question.question_id, body).subscribe({
      next: data => {
        this.refreshQuestion();
        alert("Sửa câu hỏi thành công");
      },
      error: error => {
        alert("Đã xảy ra lỗi, vui lòng thực hiện lại");
        console.log(error);
      }
    })
  }

  deleteQuestion(question: Question) {
    this.questionService.deleteQuestion(question.question_id).subscribe({
      next: data => {
        this.refreshQuestion();
        alert("Xóa câu hỏi thành công");
      },
      error: error => {
        alert("Đã xảy ra lỗi, vui lòng thực hiện lại");
      }
    });
  }
}
