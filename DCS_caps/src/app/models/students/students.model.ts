export class Students {
  student_id: String;
  class_id: String;
  number_of_member: String;
  student_full_name: String;
  birthday: String;
  email: String;
  address: String;
  phone: bigint;
}
