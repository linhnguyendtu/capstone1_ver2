export class Question {
    question_id: string;
    parent_id: string;
    class_id: string;
    question_content: string;
    create_date: string;
    state: string;
}
