export class Sms {
  ApiKey: string;
  SecretKey: string;
  Content: string;
  SmsType: string;
}
