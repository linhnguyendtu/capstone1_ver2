export class Tuition {
    student_id: string;
    student_full_name: string;
    academy_year: string;
    semester: string;
    finance_name: string;
    money: string; status: string;
    create_date: string;
}
